(ns esme.callapp
  (:use [clojure.tools.logging])
  (:require [clojure.data.json :as json])
  (:require [org.httpkit.client :as http]
            [clojure.string :as str])
  (:import javacompile.SessionParams)
  (:import
           (java.util Base64)
           (org.httpkit.client TimeoutException)))


(defn encode [to-encode]
  (.encodeToString (Base64/getEncoder) (.getBytes to-encode)))

(defn callAPP
  [msisdn input sessionId newini con_input as_request_type as_url
   as_connect_timeout as_read_timeout msg_as_timeout type]

  (if (every? empty? [msisdn input sessionId as_request_type])
    (let [resp (json/write-str {:message msg_as_timeout, :action false})]
      resp)
    (let [data (if (= type "0501000101")
                 (let [_ (swap! con_input assoc (keyword msisdn)
                                (str/replace input "#" ""))]
                   (json/write-str {:rc "0" :ogn msisdn
                                  :opid "013" :ur input :config "ussd" :mk "T"
                                  :sid sessionId}))
                 (let [_ (infof "jmgObj exists %s|%s"msisdn (.sessionExists newini (str msisdn "jmgObj")))
                       ur (str (.getSessionId newini (str msisdn "ur")) "*"input)
                       jmgObj (.getSessionId newini (str msisdn "jmgObj"))]
                   (json/write-str {:rc "0" :jmgObj (when-not (nil? jmgObj)
                                                      (json/read-str jmgObj))
                                    :ogn msisdn :opid "013" :ur ur
                                    :config "ussd" :mk "T" :sid sessionId})))
          _ (infof "callApp data msisdn=%s,data=%s"msisdn data)
          options  {:socket-timeout (Integer/parseInt as_connect_timeout)
                    :conn-timeout (Integer/parseInt as_read_timeout)
                    :timeout (Integer/parseInt as_connect_timeout)             ; ms
                    :query-params {:usParams (encode data)}}
          {:keys [status body error] :as trace}  @(http/get as_url options)]
      (if error
        (let [error-msg (condp instance? error
                          TimeoutException ":timeout-on-connect"
                          error)
              resp (json/write-str {:message msg_as_timeout, :action false})
              _ (swap!
                  con_input
                  dissoc (keyword msisdn))]
          (errorf "Connection exception %s|%s" error-msg trace)
          (.clearSession newini (str msisdn "jmgObj"))
          (.clearSession newini (str msisdn "ur"))
          resp)

        (if (not-empty body)
          (let [jsonObj (json/read-str body :key-fn keyword)
                {:keys [jmgObj data leaf code ur] } jsonObj]
            (infof "Parsed response [%s|%s|jsonObj=%s]" msisdn body jsonObj)
            ;Parsed response [2347037875290|{"data":"Reply with \n1 to Join Group \n2 to Topup MNO\n3 to Topup Wallet\n4 to Transfer to a friend\n5 to Check Balance","ur":"*1250","leaf":false,"code":200,
            ; jmgObj":{"srcc":false,"pmg":"","mg":"","tk":"","rcc":-1,"stk":"","brcc":-1,"sbrcc":false}}
            ; |jsonObj={:data "Reply with \n1 to Join Group \n2 to Topup MNO\n3 to Topup Wallet\n4 to Transfer to a friend\n5 to Check Balance", :ur "*1250", :leaf false, :code 200,
            ; :jmgObj {:srcc false, :pmg "", :mg "", :tk "", :rcc -1, :stk "", :brcc -1, :sbrcc false}}]
            (if (or (nil? data) (empty? data))
              (let [_ (swap!
                        con_input
                        dissoc (keyword msisdn))]
                (.clearSession newini (str msisdn "jmgObj"))
                (.clearSession newini (str msisdn "ur"))
                (json/write-str {:message msg_as_timeout :action false}))
              ;{"jmgObj":{"tk":"MDcwMzQ3NDA1OTk6TW9uIFNlcCAxNCAyMzo1OTo1OSBXQVQgMjA
              ;yMA==","mg":"","pmg":"","stk":""},"code":200,"data":"Select spend limit\r\n1. N500\r\n2.
              ;N1000\r\n3. N2000\r\n4. N3000\r\n5. Another Amount","leaf":false,"ur":"*123*0"}

              (let [pjmg (json/write-str jmgObj)
                    pleaf (if (string? leaf) (read-string leaf) leaf)]
                (.sessionSave newini (str msisdn "jmgObj") (str pjmg))
                (.sessionSave newini (str msisdn "ur") ur)
                (json/write-str {:message data :action pleaf}))))
          (let [_ (swap!
                    con_input
                    dissoc (keyword msisdn))]
            (.clearSession newini (str msisdn "jmgObj"))
            (.clearSession newini (str msisdn "ur"))
            (json/write-str {:message msg_as_timeout :action false})))))))
